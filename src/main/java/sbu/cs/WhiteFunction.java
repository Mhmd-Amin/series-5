package sbu.cs;

public class WhiteFunction {
    private static String outputStr;

    public static String decode (String inputStr1, String inputStr2, int num) throws Exception {
        outputStr = "";
        if (num == 1) {
            whiteMerge(inputStr1, inputStr2);
        }
        else if (num == 2) {
            whiteReverseAndMerge(inputStr1 ,inputStr2);
        }
        else if(num == 3) {
            whiteStartEndMerge(inputStr1 ,inputStr2);
        }
        else if (num == 4) {
            whiteOddOrEven(inputStr1 ,inputStr2);
        }
        else if (num == 5) {
            white5(inputStr1 ,inputStr2);
        }
        else {
            throw new Exception("num must be between 1 and 5");
        }
        return outputStr;
    }

    private static void whiteMerge(String strData, String strData2) {
        int i = 0, j = 0;
        while (true) {
            if (i < strData.length()) {
                outputStr += strData.charAt(i);
                i++;
            }
            if (j < strData2.length()) {
                outputStr += strData2.charAt(j);
                j++;
            }
            if (i >= strData.length() && j >= strData2.length()) {
                break;
            }
        }
    }

    private static void whiteReverseAndMerge(String strData, String strData2) {
        String temp = "";
        for (int i = strData2.length() - 1; i >= 0; i--) {
            temp += strData2.charAt(i);
        }
        outputStr = strData + temp;
    }

    private static void whiteStartEndMerge(String strData, String strData2) {
        int i = 0, j = strData2.length() - 1;
        while (true) {
            if (i < strData.length()) {
                outputStr += strData.charAt(i);
                i++;
            }
            if (j >= 0) {
                outputStr += strData2.charAt(j);
                j--;
            }
            if (j < 0 && i >= strData.length()) {
                break;
            }
        }
    }

    private static void whiteOddOrEven(String strData, String strData2) {
        if (strData.length() % 2 == 0) {
            outputStr = strData;
        }
        else {
            outputStr = strData2;
        }
    }

    private static void white5(String strData, String strData2) {
        int i = 0, j = 0;
        while(true) {
            if (i < strData.length() && j < strData2.length()) {
                outputStr += Character.toString((char)((strData.charAt(i) + strData2.charAt(j) - 'a' - 'a')% 26 + 'a'));
                i++;
                j++;
            }
            else if(i < strData.length() && j >= strData2.length()) {
                outputStr += Character.toString(strData.charAt(i));
                i++;
            }
            else if (i >= strData.length() && j < strData2.length()) {
                outputStr += Character.toString(strData2.charAt(j));
                j++;
            }
            else {
                break;
            }
        }
    }
}
