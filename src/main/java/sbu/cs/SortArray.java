package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0; i < size - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < size; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            if (minIndex == i) {
                continue;
            }
            swap(arr, i, minIndex);
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 1; i < size; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    swap(arr, j, j - 1);
                }
                else {
                    break;
                }
            }
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if (size < 2) {
            return arr;
        }
        int mid = size / 2;
        int[] left = new int[mid];
        int[] right = new int[size - mid];
        for (int i = 0; i < mid; i++) {
            left[i] = arr[i];
            System.out.println(left[i]);
        }
        for (int i = mid; i < size; i++) {
            right[i - mid] = arr[i];
            System.out.println(right[i - mid]);
        }
        mergeSort(left, mid);
        mergeSort(right, size - mid);

        merge(arr, left, right);
        return arr;
    }

    private void merge(int[] arr, int[] left, int[] right) {
        int i = 0, l = 0, r = 0;
        while (l < left.length && r < right.length) {
            if (left[l] <= right[r]) {
                arr[i] = left[l];
                l++;
            }
            else {
                arr[i] = right[r];
                r++;
            }
            i++;
        }
        while (l < left.length) {
            arr[i] = left[l];
            l++;
            i++;
        }
        while (r < right.length) {
            arr[i] = right[r];
            r++;
            i++;
        }
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int start = 0, end = arr.length - 1, mean;
        while (true) {
            mean = (start + end) / 2;
            if (value == arr[mean]) {
                return mean;
            }
            else if (start >= end) {
                break;
            }
            else if (value < arr[mean]) {
                end = mean - 1;
            }
            else if(value > arr[mean]){
                start = mean + 1;
            }
        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return findRecursive(0, arr.length, value, arr);
    }

    public int findRecursive(int start, int end, int value, int[] arr) {
        int mean = (start + end)/2;
        if (value == arr[mean]) {
            return mean;
        }
        else if (start >= end) {
            return -1;
        }
        else {
            if (value > arr[mean]) {
                return findRecursive(mean + 1, end, value, arr);
            }
            return findRecursive(start, mean - 1, value, arr);
        }
    }

    public void swap(int[] arr, int firstIndex, int secondIndex) {
        int temp = arr[firstIndex];
        arr[firstIndex] = arr[secondIndex];
        arr[secondIndex] = temp;
    }
}
