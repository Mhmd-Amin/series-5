package sbu.cs;

public class BlueSubMachine extends Machine {
    private int code;

    public BlueSubMachine(int num) {
        code = num;
    }

    public void setInputStr1(String inputStr1) {
        try {
            super.setRightOutput(BlackFunction.decode(inputStr1, code));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setInputStr2(String inputStr2) {
        try {
            super.setDownOutput(BlackFunction.decode(inputStr2, code));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

