package sbu.cs;

public class PinkSubMachine extends Machine {
    public PinkSubMachine(String input1, String input2, int num) {
        String output = null;
        try {
            output = WhiteFunction.decode(input1, input2, num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.setRightOutput(output);
        super.setDownOutput(output);
    }
}