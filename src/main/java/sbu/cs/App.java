package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        return read(n, arr, input);
    }

    private static String read(int n, int[][] arr, String input) {
        Machine[][] machine = new Machine[n][n];
        for(int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) {
                    machine[i][j] = new GreenSubMachine(input, arr[i][j]);
                }
                else if (i == 0 && j < n - 1) {
                    machine[i][j] = new GreenSubMachine(machine[i][j - 1].getRightOutput(), arr[i][j]);
                }
                else if (i < n - 1 && j == 0) {
                    machine[i][j] = new GreenSubMachine(machine[i - 1][j].getDownOutput(), arr[i][j]);
                }
                else if (i == 0 && j == n - 1) {
                    machine[i][j] = new YellowSubMachine(machine[i][j - 1].getDownOutput(), arr[i][j]);
                }
                else if (i == n - 1 && j == 0) {
                    machine[i][j] = new YellowSubMachine(machine[i - 1][j].getRightOutput(), arr[i][j]);
                }
                else if ((i > 0 && j == n - 1) || (i == n - 1 && j > 0)) {
                    machine[i][j] = new PinkSubMachine(machine[i][j - 1].getRightOutput(), machine[i - 1][j].getRightOutput(), arr[i][j]);
                }
                else {
                    BlueSubMachine bp = new BlueSubMachine(arr[i][j]);
                    machine[i][j] = bp;
                    bp.setInputStr1(machine[i][j - 1].getRightOutput());
                    bp.setInputStr2(machine[i - 1][j].getDownOutput());
                }
            }
        }
        return machine[n - 1][n - 1].getRightOutput();
    }
}
