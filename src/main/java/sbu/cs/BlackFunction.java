package sbu.cs;

public class BlackFunction {
    private static String outputStr;

    public static String decode (String inputStr, int num) throws Exception {
        outputStr = "";
        if (num == 1) {
            blackReverser(inputStr);
        }
        else if (num == 2) {
            blackRepeater(inputStr);
        }
        else if(num == 3) {
            blackDuplicate(inputStr);
        }
        else if (num == 4) {
            blackRightShift(inputStr);
        }
        else if (num == 5) {
            black5(inputStr);
        }
        else {
            throw new Exception("num must be between 1 and 5");
        }
        return outputStr;
    }

    private static void blackReverser(String strData) {
        for (int i = strData.length() - 1; i >= 0; i--) {
            outputStr += strData.charAt(i);
        }
    }

    private static void blackRepeater(String strData) {
        for (int i = 0; i < strData.length(); i++) {
            for (int j = 0; j < 2; j++) {
                outputStr += strData.charAt(i);
            }
        }
    }

    private static void blackDuplicate(String strData) {
        outputStr = strData + strData;
    }

    private static void blackRightShift(String strData) {
        outputStr = (strData.charAt(strData.length() - 1) + strData.substring(0, strData.length() - 1));
    }

    private static void black5(String strData) {
        for (int i = 0; i < strData.length(); i++) {
            char tempChar = strData.charAt(i);
            if (Character.isLowerCase(tempChar)) {
                outputStr += Character.toString((char) ('z' - (tempChar - 'a')));
            }
            else {
                System.err.println("string must be small letter");
                System.exit(0);
            }
        }
    }
}
