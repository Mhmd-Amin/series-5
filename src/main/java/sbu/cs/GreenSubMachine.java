package sbu.cs;

public class GreenSubMachine extends Machine {
    public GreenSubMachine(String input, int num) {
        String output = null;
        try {
            output = BlackFunction.decode(input, num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.setRightOutput(output);
        super.setDownOutput(output);
    }
}

