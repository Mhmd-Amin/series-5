package sbu.cs;

public class YellowSubMachine extends Machine {
    public YellowSubMachine(String input, int num) {
        String output = null;
        try {
            output = BlackFunction.decode(input, num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.setRightOutput(output);
        super.setDownOutput(output);
    }
}