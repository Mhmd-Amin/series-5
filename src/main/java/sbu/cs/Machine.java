package sbu.cs;

public abstract class Machine {
    private String rightOutput = "";
    private String downOutput = "";

    public Machine() {}

    protected void setRightOutput(String strData) {
        this.rightOutput = strData;
    }

    protected void setDownOutput(String strData) {
        this.downOutput = strData;
    }

    public String getRightOutput() {
        return rightOutput;
    }

    public String getDownOutput() {
        return downOutput;
    }
}

